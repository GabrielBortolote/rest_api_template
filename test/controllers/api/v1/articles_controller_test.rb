require 'test_helper'
require 'json'

class Api::V1::ArticlesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @one = articles(:one)
  end

  test "sucessful index" do
    get api_v1_articles_path
    articles = JSON.parse(@response.body)["data"]
    assert_equal 50, articles.length
  end

  test "sucessful show" do
    get api_v1_article_path(@one)
    article = JSON.parse(@response.body)["data"]
    assert_equal "Fixture One", article["title"]
  end

  test "sucessful update" do
    title = "Edited Title"
    body = "Edited body my dear little boy"
    patch api_v1_article_path(@one), params: {title: title, body: body}
    message = JSON.parse(@response.body)["message"]

    @one.reload
    assert_equal title, @one.title
    assert_equal body, @one.body
    assert_equal assigns(:sucessful_update_message), message
  end

  test "sucessful create" do
    title = "Created Book"
    body = "Created Book sucessful"
    post api_v1_articles_path, params: {title: title, body: body}
    message = JSON.parse(@response.body)["message"]
    article = Article.find_by(title: title)

    assert_not_nil article
    assert_equal title, article.title
    assert_equal assigns(:sucessful_create_message), message

    article.reload
    assert_equal title, article.title
  end

  test "sucessful delete" do
    delete api_v1_article_path(@one)
    article = Article.find_by(id: @one.id)

    assert_nil article
  end

  test "unsucessful create" do
    post api_v1_articles_path, params: {title: "  ", body: "   "}
    message = JSON.parse(@response.body)["message"]

    assert_not_nil message
    assert_equal assigns(:error_create_message), message
  end

  test "unsucessful update" do
    patch api_v1_article_path(@one), params: {title: "  ", body: "   "}
    message = JSON.parse(@response.body)["message"]

    assert_not_nil message
    assert_equal assigns(:error_update_message), message

  end  
end
