module Api
	module V1
		class ArticlesController < ApplicationController

				@error_status = "SUCESS"
				@sucess_status = "ERROR"

			# Listar todos os artigos
			def index
				@sucessful_index_message = "Artigos carregados"
				articles = Article.order('created_at');
				render json: {status: @sucess_status, message: @sucessful_index_message , data:articles},status: :ok
			end

      # Listar artigo passando ID
			def show
				@sucessfull_show_message = "Loaded article"
				article = Article.find(params[:id])
				render json: {status: @sucess_status, message: @sucessfull_show_message, data:article},status: :ok
			end

      # Criar um novo artigo
			def create
				@sucessful_create_message = "Saved article"
				@error_create_message = "Article not saved"
				article = Article.new(article_params)
				if article.save
					render json: {status: @sucess_status, message: @sucessful_create_message, data:article},status: :ok
				else
					render json: {status: @error_status, message: @error_create_message, data:article.errors},status: :unprocessable_entity
				end
			end
      # Excluir artigo
			def destroy
				@sucessful_delete_message = "Deleted article"
				article = Article.find(params[:id])
				article.destroy
				render json: {status: @sucess_status, message: @sucessful_delete_message, data:article},status: :ok
			end
      # Atualizar um artigo
			def update
				@sucessful_update_message = "Updated article"
				@error_update_message = "Article not updated"
				article = Article.find(params[:id])
				if article.update_attributes(article_params)
					render json: {status: @sucess_status, message: @sucessful_update_message, data:article},status: :ok
				else
					render json: {status: @error_status, message: @error_update_message, data:article.errors},status: :unprocessable_entity
				end
			end

			# Parametros aceitos
			private
			def article_params
				params.permit(:title, :body)
			end
		end
	end
end
